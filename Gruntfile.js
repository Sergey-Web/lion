module.exports = function(grunt) {

	require("load-grunt-tasks")(grunt);

	grunt.initConfig({

		clean: {
			build:["build"]
		},

		less: {
			style: {
				files: {
					"source/css/style.css": ["source/less/style.less"]
				}
			}
		},

		autoprefixer: {
			options: {
				browsers: ["last 2 version", "ie 9", "ie 8"]
			},
			files: {
				src: "source/css/style.css"
			}
		},

		cssmin: {
			options: {
				shorthandCompacting: false,
				choices: "gzip"
			},
			style: {
				files: {
					"build/css/style.min.css": ["build/css/style.css"]
				}
			}
		},

		csscomb: {
			style: {
				expand: true,
				src: ["build/css/style.css"]
			}
		},

		imagemin: {
			images: {
				options: {
					optimizationLevel: 3
				},
				files: [{
					expand: true,
					src: ["source/img/*/*.{png,jpg,gif,svg}"]
				}]
			}
		},

		copy: {
			build: {
				files: [{
					expand: true,
					cwd: "source",
					src: [
					"img/**",
					"scripts/**",
					"index.html"
					],
					dest: "build"
				}]
			}
		},

		cmq: {
			style: {
				files: {
					"source/css/style.css": ["source/css/style.css"]
				}
			}
		},

		watch: {
			style: {
				files: "source/less/*.less",
				tasks: ["less", "autoprefixer", "cmq"],
				options: {
					interrupt: true,
				}
			}
		}

	});


	grunt.registerTask("build", [
		"clean",
		"copy",
		"less",
		"autoprefixer",
		"cmq",
		"cssmin",
		"csscomb",
		"imagemin",
		"watch"
	]);

};